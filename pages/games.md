# Games

## Free

- [Xonotic](https://xonotic.org/) : A free fast fps

## Games tools
- [Parsec](https://parsec.app/features) : A nice tool for playing with your friends offline multiplayer games

- [jam.gg](https://jam.gg/en/) : An interesting website for playing good old gems even in multiplayer 