# Development tools

## Design
- [Lunacy](https://icones8.fr/lunacy)  : A free offline mockup software.
- [design-resources-for-developers](https://github.com/bradtraversy/design-resources-for-developers) : A vast github repository with a lot of useful references
- [Feather Icons](https://feathericons.com/) : Free open source icon library

## Graphics 
- [SvgPathEditor](https://yqnn.github.io/svg-path-editor/) : A nice online SVG editor
- [0to255](https://0to255.com/) : A really nice color picker
- [Coolors](https://coolors.co/) : A fine tool to create colour palettes

## Unix
- [Pure SH Bible](https://github.com/dylanaraps/pure-sh-bible) : As named a helpful sh bible

## Tools
- [Framasoft](https://framasoft.org/) : A freeware website really useful to find alternative and great new tools

- [Online tools for developers](https://tools.9revolution9.com/) : Some good online tools

## Website
- [Hacker news](https://news.ycombinator.com/) : A general website for sharing technology and science related content.
- [Faceslog](https://faceslog.com/) : A great blog with really interesting systems papers
- [Medium](https://medium.com/) : A melting-pot of articles about very different subjects
- [Binomed docs](https://jef.binomed.fr/binomed_docs/) : A list of slide about different topics (android, web...)
- [Visual design rules](https://anthonyhobday.com/sideprojects/saferules/) : A nice list of different advice about visual design

Books : 
- Design of everyday things, Don Norman
