# Game development tools

## Music
- [Bosca Ceoil](https://boscaceoil.net/) : Free minimalist music creation software.


## Graphics

- [Kenney](https://kenney.itch.io/) : A lot of different graphics tools of great quality and easy to use!

- [picoCAD](https://johanpeitz.itch.io/picocad) *paid* : A tiny lowpoly 3D models texturing tool. 
- [Poly Haven](https://polyhaven.com/) : Free et CC0 HDRIs, Textures and models.
- [Textures.com](https://www.textures.com/) *paid* : As the title says, a lot of textures !

## Blender
- [Starship Generator](https://github.com/a1studmuffin/SpaceshipGenerator) : A nice plugin to generator starships in blender

## Pixel art
- [Aseprite](https://www.aseprite.org/) *paid* : A really nice pixel art tool, easy to use.
    - [MortMort Aseprite Dark Mode](https://mortmort.itch.io/aseprite-darkmode) : A clear and nicely done dark mode for Aseprite
- [The spriters resource](https://www.spriters-resource.com/) : A lot of different resources regarding sprites

## Tools
- [Tiny Tools](https://tinytools.directory/) : Open source, experimental and tiny tools. A good place to find new tools depending on your needs.
- [Itch.io](https://itch.io/) : A wonderful website where you can find video games and tools of all genre 
- [Ambrosine](http://www.ambrosine.com/resource.php) : A website full of ressources for creating video games but also about retrogaming 
- [Eye Candt](https://eycndy.com/) : A nice video reference library to find inspiration 


## Website
- [Game Developer](https://www.gamedeveloper.com/)  : A lot of really interesting articles about the video game industry.
- [Unity Blog](https://blog.unity.com/) : Blog related to Unity game Engine.

## Strategy
- [Game template strategy](https://dkrikun.files.wordpress.com/2016/11/game-templatea-strategy.pdf)

Books : 
Real-Time collision detection, Christer Ericson.
